<div id="block-<?php print $block->module .'-'. $block->delta; ?>" class="clear-block block block-<?php print $block->module ?>	">

<?php
//begin part to install
if (user_access('modify blockcss')) :?>
<a href='<?php print check_url(base_path()) ?>admin/settings/cssblockeditor/<?php print $block->bid;?>'>Edit CSS <?php print $block->title;?></a>
    <?php
    endif;
//end part to install
?>

<?php if (!empty($block->subject)): ?>
  <h2><?php print $block->subject ?></h2>
<?php endif;?>

  <div class="content"><?php print $block->content ?></div>
</div>