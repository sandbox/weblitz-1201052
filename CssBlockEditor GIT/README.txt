
== Description ==
css_block_editor provides a css editor to custom css of user defined blocks.

== Installation ==

1. Enable the module

2. Copy templates/block.tpl.php file in your theme templates directory (yourtheme/templates/block.tpl.php) or paste
the following link in your block.tpl.php

<?php
//begin part to install
if (user_access('modify blockcss')) :?>
<a href='<?php print check_url(base_path()) ?>admin/settings/css_block_editor/<?php print $block->bid;?>'>Edit CSS <?php print $block->title;?></a>
    <?php
    endif;
//end part to install
?>

== Permissions ==
The module defines "modify blockcss" permission

== Usage ==

Click on "Edit CSS" link which appears on the block in view mode.
Edit custom css of the block.
(Note: In the editor you can use "@blockid" string; it will automatically replaced 
with # and the current css id value of the edited block (@blockid -> #block-block-1)).
Click on "Save css" button to save your custom css and refresh generated css. 

== Credits ==

www.weblitz.it 